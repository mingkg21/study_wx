//app.js

App({
  onLaunch: function () {
    
        //  getApp().globalData.header.Cookie = 'PHPSESSID=' + res.data;
    
    
    // 展示本地存储能力
    var logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)

    var that = this

    // 登录
    wx.login({
      success: res => {
        console.log('login res ' + res)
        if (res.code) {
          console.log('登录成功！' + res.errMsg)
        } else {
          console.log('登录失败！' + res.errMsg)
        }
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
      }
    })
    // 获取用户信息
    wx.getSetting({
      success: res => {
        console.log('getSetting res ' + res)
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            success: res => {
              console.log('getUserInfo res ' + res)
              // 可以将 res 发送给后台解码出 unionId
              this.globalData.userInfo = res.userInfo

              // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
              // 所以此处加入 callback 以防止这种情况
              if (this.userInfoReadyCallback) {
                this.userInfoReadyCallback(res)
              }
            }
          })
        }
      }
    })

    wx.getStorage({
      key: 'selected_lib_id',
      success: function(res) {
        if (res.data) {
          that.globalData.selectedLibId = res.data
        } else {
          console.log('lib id: xxx ' + res.data)
        }
      },
    })
    wx.getStorage({
      key: 'libs_key',
      success: function(res) {
        if (res.data) {
          that.globalData.libs = res.data
          if (that.globalData.selectedLibId) {
            res.data.forEach(function (e) {
              if (e.id === that.globalData.selectedLibId) {
                that.globalData.selectedLib = e
              }
            })
          } else {
            that.globalData.selectedLibId = res.data[0].id
          }
        }
      },
    })
  },

  globalData: {
    header: { 'Cookie': '' },
    httpUrl:'https://wx.fjmdjy.com/',
    src:'https://wx.fjmdjy.com/attachment/',
    selectedLibId: 0,
    selectedLib: null,
    libs: [],

    // httpUrl: 'https://wx.songqintest.com/',
    // src: 'https://wx.songqintest.com/attachment/'
  },

  setSelectedLib: function(lib) {
    console.log('setSelectedLibId libId: ' + lib.id)
    this.globalData.selectedLib = lib
    this.globalData.selectedLibId = lib.id
    console.log('setSelectedLibId: ' + this.globalData.selectedLibId)
    wx.setStorage({
      key: 'selected_lib_id',
      data: this.globalData.selectedLibId,
    })
  },

  setLibs: function(libs) {
    this.globalData.libs = libs
    wx.setStorage({
      key: 'libs_key',
      data: libs,
    })
  }
}) 

