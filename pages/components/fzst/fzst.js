var sliderWidth = 96; // 需要设置slider的宽度，用于计算中间位置

Page({
  data: {
    tabs: ["专题检测", "周考月考", "仿真试题"],
    activeIndex: 0,
    sliderOffset: 0,
    sliderLeft: 0
  },
  onLoad: function () {
    var that = this;
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          sliderLeft: (res.windowWidth / that.data.tabs.length - sliderWidth) / 2,
          sliderOffset: res.windowWidth / that.data.tabs.length * that.data.activeIndex
        });
      }
    });
  },
  nihao: function (res) {
    var httpUrl = getApp().globalData.httpUrl;
    var that = this;
    var header;
    var id = res.currentTarget.dataset.id;
    console.log(res)
    wx.getStorage({
      key: 'login',
      success: function (res) {
        header = res.data;
        wx.request({
          url: httpUrl + 'app/wxapi.php?i=3&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
          data: {
            age: 288,
            id: id
          },
          header: {
            'Cookie': 'PHPSESSID=' + header
          },
          success: function (res) {
            wx.requestPayment({
              'timeStamp': res.data.timeStamp,
              'nonceStr': res.data.nonceStr,
              'package': res.data.package,
              'signType': res.data.signType,
              "paySign": res.data.paySign,
              'success': function (obj) {
                console.log(obj)
              }
            })
          }
        })
      }
    })
  },
  tabClick: function (e) {
    this.setData({
      sliderOffset: e.currentTarget.offsetLeft,
      activeIndex: e.currentTarget.id
    });
  },
  onLoad: function (options) {
    var httpUrl = getApp().globalData.httpUrl;
    var that = this;
    var header;
    wx.getStorage({
      key: 'login',
      success: function (res) {
        header = res.data;
        wx.request({
          url: httpUrl + 'app/wxapi.php?i=3&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
          data: {
            age: 284,
            page: 1,
            cateid: 2,
            libid: getApp().globalData.selectedLibId
          },
          header: {
            'Cookie': 'PHPSESSID=' + header
          },
          success: function (res) {
            that.setData({
              shuliang: res.data.data
            })
          }
        })
        // wx.request({
        //   url: httpUrl + 'app/wxapi.php?i=3&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
        //   data: {
        //     age: 284,
        //     page: 1,
        //     cateid: 3
        //   },
        //   header: {
        //     'Cookie': 'PHPSESSID=' + header
        //   },
        //   success: function (res) {
        //     that.setData({
        //       zkyk: res.data.data
        //     })
        //   }
        // })
        // wx.request({
        //   url: httpUrl + 'app/wxapi.php?i=3&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
        //   data: {
        //     age: 284,
        //     page: 1,
        //     cateid: 4
        //   },
        //   header: {
        //     'Cookie': 'PHPSESSID=' + header
        //   },
        //   success: function (res) {
        //     that.setData({
        //       fzst: res.data.data
        //     })
        //   }
        // })
      }
    })
  },
  kaos: function (e) {
    console.log(e)
    var id = e.currentTarget.dataset.id
    wx.navigateTo({
      url: '../mryl/mryl?id=' + id + '&name=fzst' + '&examtype=' + 2,
    })
  },
});