// pages/xiangqing/xiangqing.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    shuliang: 1,
    minusStatus: 'disabled',
    productprice: '',

    id: '',
    title:'',
    thumb:'',
    productprice:'',
    id:'',
    marketprice1:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    console.log(options)
    var src = getApp().globalData.src;
    var that = this;
    
    that.setData({
      imgUrl: src
    })
    this.setData({
      title: options.title,
      thumb: options.thumb,
      productprice: options.productprice,
      id: options.id,
      marketprice1: options.marketprice1,
      
    })
  },
  zhifu: function() {
    var that = this;
    var app = getApp();
    var header;
    var httpUrl = app.globalData.httpUrl;
    wx.getStorage({
      key: 'login',
      success: function(res) {
        header = res.data;
        wx.request({
          url: httpUrl + 'app/wxapi.php?i=3&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
          data: {
            age: 245,
            goodsid: that.data.id,
            good_number: that.data.shuliang
          },
          header: {
            'Cookie': 'PHPSESSID=' + header
          },
          success: function(res) {
            console.log(res)
            wx.requestPayment({
              'timeStamp': res.data.timeStamp,
              'nonceStr': res.data.nonceStr,
              'package': res.data.package,
              'signType': 'MD5',
              'paySign': res.data.paySign,
              'success': function(obj) {
                // console.log("123213"+obj)
              },
              'fail': function(res) {
                var that = this;
                var app = getApp();
                var httpUrl = app.globalData.httpUrl;
                // console.log(res)
                wx.request({
                  url: httpUrl + 'app/wxapi.php?i=3&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
                  data: {
                    age: 246,
                    errMsg: res.errMsg
                  },
                  success: function(res) {
                    console.log(res)
                  }
                })
              }
            })
            // console.log(res);
          }
        })
      }
    })

  },

  jia: function() {
    var shuliang = this.data.shuliang;
    var productprice = this.data.productprice;
    var marketprice1 = this.data.marketprice1
    shuliang++;
    var minusStatus = shuliang < 1 ? 'disabled' : 'normal';
    this.setData({
      shuliang: shuliang,
      minusStatus: minusStatus,
      productprice: marketprice1 * shuliang
    });
  },
  jian: function() {
    var shuliang = this.data.shuliang;
    var productprice = this.data.productprice
    if (shuliang > 1) {
      shuliang--
    
    var minusStatus = shuliang <= 1 ? 'disabled' : 'normal';
      var marketprice1 = this.data.marketprice1
    this.setData({
      shuliang: shuliang,
      minusStatus: minusStatus,
      productprice: productprice - marketprice1
    });
    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    var app = getApp();
    var httpUrl = app.globalData.httpUrl;
    var that = this;
    var header;
    wx.getStorage({
      key: 'login',
      success: function(res) {
        header = res.data;
        wx.request({
          url: httpUrl+'app/wxapi.php?i=3&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
          data: {
            age: 235
          },
          header: {
            'Cookie': 'PHPSESSID=' + header
          },
          success: function(res) {
            var dzid = res.data.code
            console.log(res)
            that.setData({
              dzid: res.data.code,
              realname: res.data['0'].realname,
              mobile: res.data['0'].mobile,
              province: res.data['0'].province,
              city: res.data['0'].city,
              area: res.data['0'].area,
              address: res.data['0'].address
            })
          }
        })
      }
    })
  },
  shdz: function() {
    var title = this.data.title;
    var thumb = this.data.thumb;
    var productprice = this.data.productprice;
    var id = this.data.id;
    var marketprice1 = this.data.marketprice1
    wx.navigateTo({
      url: '../dizhi/dizhi?arrid=' + 1 + '&title=' + title + '&thumb=' + thumb + '&productprice=' + productprice + '&id=' + id + '&marketprice1=' + marketprice1,
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})