// pages/components/welcome/welcome.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    detail: null,
    modalHidden: true, //是否隐藏对话框
    name: '',
    pwd: '',
  },
  userInfoHandler: function(e) {
    console.log(e);
    var that = this;
    that.setData({
      detail: e.detail
    })
    wx.setStorage({
      key: 'key',
      data: this.data.detail,
    })

    wx.login({
      success: res => {
        var httpUrl = getApp().globalData.httpUrl;
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
        if (res.code) {
          var that = this;
          wx.showLoading({
            title: '登录中...',
          })
          wx.request({
            url: httpUrl + 'app/wxapi.php?i=3&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
            method: "GET",
            data: {
              "code": res.code,
              age: 241,
              detail: this.data.detail,
            },
            header: {
              'content-type': 'application/json' // 默认值
            },
            success: function(res) {
              wx.hideLoading()
              if (res.data === '用户未绑定') {
                console.log("用户未绑定，弹出对话框")
                that.setData({
                  modalHidden: false
                })
              } else {
                if (res.data.openid) {
                  wx.setStorage({
                    key: "tokenId",
                    data: res.data.openid
                  })
                }
                wx.setStorage({
                  key: 'login',
                  data: res.data,
                })
                wx.setStorage({
                  key: 'sessionId',
                  data: 'JSESSIONID=' + res.data,
                  success: function(res) {
                    console.log(res)
                  }
                })
                wx.switchTab({
                  url: "../../index/index",
                })
              }

            }
          })
          // if (e.detail.errMsg == 'getPhoneNumber:ok') {
          //   wx.switchTab({
          //     url: "../../index/index",
          //   })
          // } else {
          //   wx.switchTab({
          //     url: "../../index/index",
          //   })
          // }
        }

      }
    })

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    wx.getStorage({
      key: 'login',
      success: function(res) {
        console.log('onLoad' + res.data)
        if (res.data !== '') {
          wx.switchTab({
            url: "../../index/index",
          })
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    // console.log(this.data.detail)
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },

  bindNameInput(e) {
    this.setData({
      name: e.detail.value
    })
  },

  bindPwdInput(e) {
    this.setData({
      pwd: e.detail.value
    })
  },

  //确定按钮点击事件
  modalBindaconfirm: function() {
    //先判断输入框空值
    if (this.data.name === '' || this.data.pwd === '') {
      wx.showToast({
        title: '学号或密码不能为空！',
        icon: 'none',
        duration: 2000
      })
      return
    }
    this.setData({
      modalHidden: !this.data.modalHidden,
    })
    wx.showLoading({
      title: '账户绑定中...',
    })
    console.log('name: ' + this.data.name)
    console.log('pwd: ' + this.data.pwd)
    wx.login({
      success: res => {
        if (res.code) {
          var that = this;
          var httpUrl = getApp().globalData.httpUrl;
          wx.request({
            url: httpUrl + 'app/wxapi.php?i=3&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
            method: "GET",
            data: {
              "code": res.code,
              age: 2241,
              detail: this.data.detail,
              name: this.data.name,
              pwd: this.data.pwd
            },
            header: {
              'content-type': 'application/json' // 默认值
            },
            success: function(res) {
              wx.hideLoading()
              if (res.data === '账号或密码错误') {
                wx.showToast({
                  title: res.data,
                  icon: 'none',
                  duration: 2000
                })
                that.setData({
                  modalHidden: false
                })
              } else {
                if (res.data.openid) {
                  wx.setStorage({
                    key: "tokenId",
                    data: res.data.openid
                  })
                }
                wx.setStorage({
                  key: 'login',
                  data: res.data,
                })
                wx.setStorage({
                  key: 'sessionId',
                  data: 'JSESSIONID=' + res.data,
                  success: function(res) {
                    console.log(res)
                  }
                })
                wx.switchTab({
                  url: "../../index/index",
                })
                
              }
            }
          })
        }
      }
    })
  },
  //取消按钮点击事件
  modalBindcancel: function() {
    this.setData({
      modalHidden: !this.data.modalHidden,
    })
  }

})