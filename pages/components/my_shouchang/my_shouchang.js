var sliderWidth = 200; // 需要设置slider的宽度，用于计算中间位置

Page({
  data: {
    tabs: ["收藏课程", "收藏资料"],
    activeIndex: 0,
    sliderOffset: 0,
    sliderLeft: 0,
  },
  onLoad: function () {
    var that = this;
    var src = getApp().globalData.src;
    that.setData({
      src: src
    })
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          sliderLeft: "0",
          sliderOffset: res.windowWidth / that.data.tabs.length * that.data.activeIndex
        });
      }
    });
  },
  onReady: function () {
    var app = getApp();
    var httpUrl = app.globalData.httpUrl;
    var header;
    var that = this;
    wx.getStorage({
      key: 'login',
      success: function (res) {
        header = res.data;
        wx.request({
          url: httpUrl + 'app/wxapi.php?i=3&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
          data: {
            age: 253,
            goodstype: 6
          },
          header: {
            'Cookie': 'PHPSESSID=' + header
          },
          success: function (res) {
            that.setData({
              shuliang: res.data.message
            })
          }
        })
        wx.request({
          url: httpUrl + 'app/wxapi.php?i=3&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
          data: {
            age: 253,
            goodstype: 1
          },
          header: {
            'Cookie': 'PHPSESSID=' + header
          },
          success: function (res) {
            console.log(res)
            that.setData({
              shuliang1: res.data.message
            })
          }
        })

      }
    })
  },
  toaozhuan: function (obj) {
    // console.log(obj)
    var id = obj.currentTarget.dataset.id;
    wx.navigateTo({
      url: '../xiangqing/xiangqing?arrid=' + id
    })
  },
  tabClick: function (e) {
    this.setData({
      sliderOffset: e.currentTarget.offsetLeft,
      activeIndex: e.currentTarget.id
    });
  }
});