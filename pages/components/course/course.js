var app = getApp();
var index;
var nav_centent_list = [
  ['语文', '数学', '英语'],
  ['章节', '章节', '章节', '章节'],
  ['综述', '综述']
];
Page({
  data: {
    nav_title: ['一级菜单', '二级菜单', '三级菜单'],
    shownavindex: null,
    nav_centent: null,
    xiaoshi: true,
    yidia: 1,
    name: '请点击选择题库',
    dashuliang: ''
  },
  onReady: function() {
    // var app = getApp();
    // var header;
    // var httpUrl = app.globalData.httpUrl;
    // var that = this;
    // var src = app.globalData.src;
    // wx.getStorage({
    //   key: 'login',
    //   success: function(res) {
    //     header = res.data;
    //     wx.request({
    //       url: httpUrl + 'app/wxapi.php?i=3&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
    //       header: {
    //         'Cookie': 'PHPSESSID=' + header
    //       },
    //       data: {
    //         age: 272
    //       },
    //       success: function(res) {
    //         that.setData({
    //           // name: res.data.data1.name,
    //           // name1: res.data.data2.name,
    //           // name2: res.data.data3.name,
    //           // dashuliang: res.data.data_course
    //         })
    //       }
    //     })
    //   }
    // })
  },
  toaozhuan: function(obj) {
    // console.log(obj)
    var id = obj.currentTarget.dataset.id;
    wx.navigateTo({
      url: '../xiangqing/xiangqing?arrid=' + id
    })
  },
  onLoad: function() {
    var app = getApp();
    var header;
    var httpUrl = app.globalData.httpUrl;
    var that = this;
    var src = app.globalData.src;
    that.setData({
      src: src
    })
    wx.getStorage({
      key: 'login',
      success: function(res) {
        header = res.data;
        wx.request({
          url: httpUrl + 'app/wxapi.php?i=3&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
          data: {
            age: 269
          },
          header: {
            'Cookie': 'PHPSESSID=' + header
          },
          success: function(res) {
            console.log(res)
            that.setData({
              shuliang: res.data,
              name: res.data['0'].name
            })
          }
        })

      }
    })
  },
  mingz: function(e) {
    this.setData({
      name: e.currentTarget.dataset.name,
      shuliang3: [],
      name2: ''
    })
    var app = getApp();
    var header;
    var httpUrl = app.globalData.httpUrl;
    var that = this;
    var id = e.currentTarget.dataset.id
    wx.getStorage({
      key: 'login',
      success: function(res) {
        header = res.data;
        wx.request({
          url: httpUrl + 'app/wxapi.php?i=3&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
          data: {
            age: 270,
            id: id
          },
          header: {
            'Cookie': 'PHPSESSID=' + header
          },
          success: function(res) {
            console.log(res);
            that.setData({
              shuliang2: res.data,
              yidia: 2
            })
          }
        })
      }
    })
  },
  mingz2: function(e) {
    this.setData({
      name1: e.currentTarget.dataset.name,
    })
    var app = getApp();
    var header;
    var httpUrl = app.globalData.httpUrl;
    var that = this;
    var id = e.currentTarget.dataset.id
    wx.getStorage({
      key: 'login',
      success: function(res) {
        header = res.data;
        wx.request({
          url: httpUrl + 'app/wxapi.php?i=3&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
          data: {
            age: 270,
            id: id
          },
          header: {
            'Cookie': 'PHPSESSID=' + header
          },
          success: function(res) {
            console.log(res);
            that.setData({
              shuliang3: res.data,
              yidia: 3
            })
          }
        })
      }
    })
  },
  mingz3: function(e) {
    this.setData({
      name2: e.currentTarget.dataset.name,
    })
    var app = getApp();
    var header;
    var httpUrl = app.globalData.httpUrl;
    var that = this;
    var id = e.currentTarget.dataset.id
    wx.getStorage({
      key: 'login',
      success: function(res) {
        header = res.data;
        wx.request({
          url: httpUrl + 'app/wxapi.php?i=3&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
          data: {
            age: 271,
            cate_type: 3,
            cate_id: id
          },
          header: {
            'Cookie': 'PHPSESSID=' + header
          },
          success: function(res) {
            console.log(res);
            that.setData({
              dashuliang: res.data,
              yidia: 0
            })
          }
        })
      }
    })
  },
  yijidian: function(e) {
    var name = this.data.name

    console.log(e)
    var that = this;
    // var index = e.
    if (that.data.yidia == e.currentTarget.dataset.index) {
      that.setData({
        yidia: 0
      })
    } else {
      that.setData({
        yidia: e.currentTarget.dataset.index
      })
    }



  },
  yijidian1: function() {
    var yidia = this.data.yidia1;
    var that = this;
    that.setData({
      yidia: true
    })
    if (yidia == true) {
      that.setData({
        yidia1: false
      })
    } else {
      that.setData({
        yidia1: true
      })
    }
  },
  yijidian2: function() {
    var yidia2 = this.data.yidia2;
    var yidia = this.data.yidia;
    var yidia1 = this.data.yidia1
    var that = this;
    that.setData({
      yidia1: true,
      yidia: true,
      yidia2: true
    })
    if (yidia2 == true) {
      that.setData({
        yidia2: false
      })
    } else {
      that.setData({
        yidia2: true
      })
    }
  },
  click_nav: function(e) {
    var xiaoshi = this.data.xiaoshi;
    if (xiaoshi == true) {
      this.setData({
        xiaoshi: false
      })
    } else {
      this.setData({
        xiaoshi: true
      })
    }
    if (index == e.currentTarget.dataset.index && this.data.nav_centent != null) {
      index = e.currentTarget.dataset.index;
      this.setData({
        nav_centent: null,
        shownavindex: null,
      })
    } else if (this.data.nav_centent == null) {
      console.log(11)
      index = e.currentTarget.dataset.index;
      this.setData({
        shownavindex: index,
        nav_centent: nav_centent_list[Number(index)]
      })
    } else {
      console.log(22)
      index = e.currentTarget.dataset.index;
      this.setData({
        shownavindex: index,
        nav_centent: nav_centent_list[Number(index)]
      })
    }
  },
  zhedangc: function(res) {
    var nav_centent = this.data.nav_centent;
    var shownavindex = this.data.shownavindex
    var xiaoshi = this.data.xiaoshi
    this.setData({
      nav_centent: null,
      xiaoshi: true,
      shownavindex: null,
    })
  },
  // onShow: function() {
  //   var app = getApp();
  //   var header;
  //   var httpUrl = app.globalData.httpUrl;
  //   var that = this;
  //   var id = e.currentTarget.dataset.id
  //   wx.getStorage({
  //     key: 'login',
  //     success: function(res) {
  //       header = res.data;
  //       wx.request({
  //         url: httpUrl + 'app/wxapi.php?i=3&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
  //         data: {
  //           age: 271,
  //           cate_type: 3,
  //           cate_id: id
  //         },
  //         header: {
  //           'Cookie': 'PHPSESSID=' + header
  //         },
  //         success: function(res) {
  //           console.log(res);
  //           that.setData({
  //             dashuliang: res.data,
  //             yidia: 0
  //           })
  //         }
  //       })
  //     }
  //   })
  // }
})