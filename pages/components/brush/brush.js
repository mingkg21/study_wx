// pages/components/brush/brush.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    yihais: false,
    jlks: "328",
    shuliang: '',
    list: [{
      id: 'form',
      name: '马克思主义哲学',
      open: false,
      pages: ['button'],
      mscy: "名师预测",
      fen: "10~20分"
    }],
    items: {
      is_like: 0,
      like: 0
    }
  },
  jinkaos: function (obj) {
    var id = obj.currentTarget.dataset.id;
    wx.navigateTo({
      url: '../mryl/mryl?libid=' + id
    })
  },
  shuatixz: function () {
    wx.navigateTo({
      url: '../bzxz/bzxz?iid=' + 1,
    })
  },
  kindToggle: function (e) {
    var is_like = "items.is_like"; //先用一个变量，把items.is_like用字符串拼接起来
    var like = "items.like";
    var yihais = this.data.yihais;

    this.setData({
      yihais: false
    })

    if (this.data.items.is_like == 0 && this.data.items.like == 0) {
      this.setData({
        [is_like]: 1,
        [like]: 1
      })
    } else if (this.data.items.is_like == 1 && this.data.items.like == 1) {

      this.setData({

        [is_like]: 0,

        [like]: 0

      })

    }
    // if(data.list.imgLIst="../images/j.png"){
    // this.setData({
    //   imgLIst:`../images/n.png`,
    // })
    // }else{
    //   this.setData({
    //     imgLIst: `../images/j.png`,
    //   })
    // }document.getElementById("dataset").style="display":"none";
    var id = e.currentTarget.id;
    var shuliang = this.data.shuliang;
    for (var i = 0, len = shuliang.length; i < len; ++i) {
      if (shuliang[i].id == id) {
        if (shuliang[i].id == id && shuliang[i].open === true) {
          shuliang[i].open = false
        } else {
          shuliang[i].open = true
        }
      } else if (shuliang[i].id !== id && shuliang[i].open === true) {
        shuliang[i].open = true
      } else {
        shuliang[i].open = false
      }
    }
    this.setData({
      shuliang: shuliang
    });
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var app = getApp();
    console.log(app);
    var that = this
    console.log(options)
    var nummber = options.goods;
    var id = options.id;
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var app = getApp();
    console.log(app);
    if(!app.globalData.back) {
      var header;
      var httpUrl = app.globalData.httpUrl;
      var src = app.globalData.src
      var that = this;
      that.setData({
        src: src
      })
      wx.getStorage({
        key: 'login',
        success: function (res) {
          header = res.data;
          wx.request({
            url: httpUrl + 'app/wxapi.php?i=3&c=site&a=entry&m=ewei_shopv2&do=app&r=students',
            header: {
              'Cookie': 'PHPSESSID=' + header
            },
            data: {
              age: 222
            },

            success: function (res) {
              // console.log(res);
              that.setData({
                challenge_data: res.data.challenge_data,
                ture_data: res.data.ture_data,
                day_data: res.data.day_data
              })
            }
          })
          wx.request({
            url: httpUrl + '/app/wxapi.php?i=3&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
            data: {
              age: 240
            },
            header: {
              'Cookie': 'PHPSESSID=' + header
            },
            success: function (res) {

              that.setData({
                name: res.data.name,
                nickname: res.data.nickname,
                avatar: res.data.avatar,
                exam_name: res.data.exam_name,
                libid_name: res.data.libid_name
              })
            }
          })
          wx.request({
            url: httpUrl + 'app/wxapi.php?i=3&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
            data: {
              age: 231,
              datatype: 1
            },
            header: {
              'Cookie': 'PHPSESSID=' + header
            },
            success: function (res) {
              console.log(res);
              that.setData({
                shuliang: res.data,
              })
            }
          })
        }
      })
    }
  },
  erjidian: function (e) {
    var that = this;
    var page = e.currentTarget.dataset.item;
    if (page.son && page.son.length > 0) {
      var yihais = that.data.yihais
      if (yihais == false) {
        that.setData({
          yihais: true
        })
      } else {
        that.setData({
          yihais: false
        })
      }
    } else {
      var id = page.id;
      wx.navigateTo({
        url: '../mryl/mryl?libid=' + id
      })
    }
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})