//获取应用实例
var app = getApp()
Page({
  data: {
    swiper:[123,12312,321,312,321,312,312]
  },
  onLoad:function(res){
    var that = this;
    // 高度自适应
    wx.getSystemInfo({
      success: function (res) {
        var clientHeight = res.windowHeight - 120,
          clientWidth = res.windowWidth,
          rpxR = 750 / clientWidth;
        var calc = clientHeight * rpxR;
        //console.log(calc)
        that.setData({
          winHeight: calc
        });
      }
    });
  }
})