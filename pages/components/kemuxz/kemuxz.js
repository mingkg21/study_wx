// pages/components/kmxz/kmxz.js
Page({
  /**
   * 页面的初始数据
   */
  data: {
    goods: '',
    iid: '',
    iiid:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    console.log(options)
    this.setData({
      goods: options.goods,
      iid: options.id,
      iiid:options.iid
    })
    var id = options.id
    var app = getApp();
    var header;
    var httpUrl = app.globalData.httpUrl;
    var that = this;
    wx.getStorage({
      key: 'login',
      success: function(res) {
        header = res.data;
        wx.request({
          url: httpUrl + 'app/wxapi.php?i=3&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
          data: {
            age: 221,
            examid: id
          },
          header: {
            'Cookie': 'PHPSESSID=' + header
          },
          success: function(res) {
            
            that.setData({
              name: res.data.exam_data
            })
          }
        })
      }
    })

  },
  changeme: function(e) {
    var goods = this.data.goods
    var iid = this.data.iid
    var id = e.currentTarget.dataset.id
    var mingz = e.currentTarget.dataset.text
    var app = getApp();
    var httpUrl = app.globalData.httpUrl;
    var that = this;
    var iiid = this.data.iiid
    var header;
    if (iiid == 0){
    wx.reLaunch({ //跳转至指定页面
      url: '../user/user?goods=' + mingz + "&id=" + id + "&dier=" + goods + "&iiid=" + iiid
    })
    }else{
      wx.reLaunch({ //跳转至指定页面
        url: '../brush/brush?goods=' + mingz + "&id=" + id + "&dier=" + goods + "&iiid=" + iiid
      })
    }
    wx.getStorage({
      key: 'login',
      success: function(res) {
        header = res.data;
        wx.request({
          url: httpUrl + 'app/wxapi.php?i=3&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
          data: {
            age: 243,
            libid: e.currentTarget.dataset.id
          },
          header: {
            'Cookie': 'PHPSESSID=' + header
          },
          success: function(res) {
          }
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
})