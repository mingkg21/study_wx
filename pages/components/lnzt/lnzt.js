// pages/components/lnzt/lnzt.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var httpUrl = getApp().globalData.httpUrl;
    var that = this;
    var header;
    wx.getStorage({
      key: 'login',
      success: function(res) {
        header = res.data;
        wx.request({
          url: httpUrl + 'app/wxapi.php?i=3&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
          data: {
            age: 284,
            page: 1,
            cateid: 1
          },
          header: {
            'Cookie': 'PHPSESSID=' + header
          },
          success: function(res) {

            that.setData({
              shuliang: res.data.data
            })
          }
        })
      }
    })
  },
  fuq: function(res) {
    var httpUrl = getApp().globalData.httpUrl;
    var that = this;
    var header;
    var id = res.currentTarget.dataset.id;
    console.log(res)
    wx.getStorage({
      key: 'login',
      success: function(res) {
        header = res.data;
        wx.request({
          url: httpUrl + 'app/wxapi.php?i=3&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
          data: {
            age: 288,
            id: id
          },
          header: {
            'Cookie': 'PHPSESSID=' + header
          },
          success: function(res) {
            wx.requestPayment({
              'timeStamp': res.data.timeStamp,
              'nonceStr': res.data.nonceStr,
              'package': res.data.package,
              'signType': res.data.signType,
              "paySign": res.data.paySign,
              'success': function(obj) {
                wx.redirectTo({
                  url: '../lnzt/lnzt',
                })
              }
            })
          }
        })
      }
    })
  },
  kaos: function(e) {
    console.log(e)
    var id = e.currentTarget.dataset.id
    wx.navigateTo({
      url: '../mryl/mryl?id=' + id + '&name=lnzt' + '&examtype=' + 1,
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})