// pages/components/jieguo/jieguo.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var app = getApp();
    var header;
    var httpUrl = app.globalData.httpUrl;
    var src = app.globalData.src
    var that = this;
    that.setData({
      src: src
    })
    wx.getStorage({
      key: 'login',
      success: function(res) {
        header = res.data;
        wx.request({
          url: httpUrl + 'app/wxapi.php?i=3&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
          data: {
            age: 287
          },
          header: {
            'Cookie': 'PHPSESSID=' + header
          },
          success: function(res) {
            console.log(res);
            that.setData({
              defen: res.data.ture,
              cuoti: res.data.false,
              shuliang:res.data.data,
              name:res.data.name,
              zongfen:res.data.all
            })
          }
        })
      }
    })
  },
  shuait:function(){
    wx.navigateBack({
      delta: 1,
    })
  },
  close: function () {
    wx.navigateBack({
      delta: 2,
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})