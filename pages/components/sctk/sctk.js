var sliderWidth = 96; // 需要设置slider的宽度，用于计算中间位置

Page({
  data: {
    tabs: ["选项一",],
    activeIndex: 0,
    sliderOffset: 0,
    sliderLeft: 0,
    isChecked: true,
    countDownNum: '0',
    xingxing: ["123", "123", "123", "123", "12312"],
    cfbytj: false,
    parameter: [{
      id: 0,
      name: '充分必要条件',
      abcd: 'A',
      current: 0,
    }, {
      id: 1,
      name: '必要不充分条件',
      abcd: 'B',
      current: 2,
    }, {
      id: 2,
      name: '充分不必要条件',
      abcd: 'C',
      current: 3,
    }, {
      id: 3,
      name: '既不充分又不必要条件',
      abcd: 'D',
      current: 4,
    }],
    timu: "在△ABC中,“cosA·cosB·cosC<0”是'△ABC为钝角三角形'的（   ）",
    parameter1: [1, 2, 3, 5, 3, 32, 424, 2],
    currentData: 0,
    pageImage: `../../../utils/img/wujiaoxing2.png`,
    pageImage1: `../../../utils/img/wujiaoxing1.png`,
    showView: false,
    isClick: false,
    isClick2: false,
    isClick3: false
  },
  onReady: function (){
    var app = getApp();
    var header;
    var httpUrl = app.globalData.httpUrl;
    var that = this;
    wx.getStorage({
      key: 'login',
      success: function (res) {
        header = res.data;
        wx.request({
          url: httpUrl + 'app/wxapi.php?i=3&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
          data: {
            age: 228
          },
          header: {
            'Cookie': 'PHPSESSID=' + header
          },
          success: function (res) {
            console.log(res)
          }
        })
      }
    })
    var httpUrl = getApp().globalData.httpUrl;

  },
  openToast: function () {
    this.setData({
      isClick2: !this.data.isClick2
    })
    wx.showToast({
      title: '已收藏',
      icon: 'success',
      duration: 3000,
    });
  },
  // 点击提交试卷
  openConfirm: function () {
    wx.showModal({
      title: '是否提交考题',
      confirmText: "确定",
      cancelText: "取消",
      success: function (res) {
        console.log(res);
        if (res.confirm) {
          console.log('用户点击确定操作')
        } else {
          console.log('用户点击取消操作')
        }
      }
    });
  },
  openTpast3: function () {
    this.setData({
      isClick3: !this.data.isClick3
    })
    if (!this.data.isChecked3 == false) {
      this.setData({
        isChecked3: false
      })
    } else {
      this.setData({
        isChecked3: true
      })
    }
  },
  openToast2: function () {
    this.setData({
      isClick: !this.data.isClick
    })
    if (!this.data.isChecked == false) {
      this.setData({
        isChecked: false
      })
    } else {
      this.setData({
        isChecked: true
      })
    }
  },
  //获取当前滑块的index
  bindchange: function (e) {
    const that = this;
    that.setData({

      currentData: e.detail.current
    })
    console.log(that.data.currentData);
  },
  parameterTap: function (e) { //e是获取e.currentTarget.dataset.id所以是必备的，跟前端的data-id获取的方式差不多

    var that = this;
    var currentData = that.data.currentData + 1;

    console.log(currentData)
    that.setData({
      currentData: currentData
    })
    var this_checked = e.currentTarget.dataset.id
    var parameterList = this.data.parameter //获取Json数组
    for (var i = 0; i < parameterList.length; i++) {
      if (parameterList[i].id == this_checked) {
        parameterList[i].checked = true; //当前点击的位置为true即选中
      } else {
        parameterList[i].checked = false; //其他的位置为false
      }
    }
    that.setData({
      parameter: parameterList
    })
    if (currentData === e.target.dataset.current) {

      return false;

    } else {

      that.setData({

      })
    }

  },
  onShow: function () {

    this.countDown();
  },
  countDown: function () {
    let that = this;
    let countDownNum = that.data.countDownNum; //获取倒计时初始值
    //如果将定时器设置在外面，那么用户就看不到countDownNum的数值动态变化，所以要把定时器存进data里面
    that.setData({
      timer: setInterval(function () { //这里把setInterval赋值给变量名为timer的变量
        //每隔一秒countDownNum就加一，实现同步
        countDownNum++;
        //然后把countDownNum存进data，好让用户知道时间在倒计着
        that.setData({
          countDownNum: countDownNum
        })
        //在倒计时还未到0时，这中间可以做其他的事情，按项目需求来
        if (countDownNum == 0) {
          //这里特别要注意，计时器是始终一直在走的，如果你的时间为0，那么就要关掉定时器！不然相当耗性能
          //因为timer是存在data里面的，所以在关掉时，也要在data里取出后再关闭
          clearInterval(that.data.timer);
          //关闭定时器之后，可作其他处理codes go here
        }
      }, 1000)
    })
  },
  onLoad: function () {
    var that = this;
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          sliderLeft: (res.windowWidth / that.data.tabs.length - sliderWidth) / 2,
          sliderOffset: res.windowWidth / that.data.tabs.length * that.data.activeIndex
        });
      }
    });
  },

  //点击切换，滑块index赋值

  tabClicks: function (e) {
    this.setData({
      isChecked: true,
      sliderOffset: e.currentTarget.offsetLeft,
      activeIndex: e.currentTarget.id
    });
  },
  tabClick: function (e) {
    this.setData({
      sliderOffset: e.currentTarget.offsetLeft,
      activeIndex: e.currentTarget.id
    });
  }
});