// pages/components/note/note.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    page:1,
    note:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onShow: function () {
    this.load()
  },
  load:function() {
    var app = getApp();
    var header;
    var httpUrl = app.globalData.httpUrl;
    var that = this;
    wx.getStorage({
      key: 'login',
      success: function (res) {
        header = res.data;
        wx.request({
          url: httpUrl + 'app/wxapi.php?i=3&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
          data: {
            age: 2255,
            num: 20,
            page: that.data.page
          },
          header: {
            'Cookie': 'PHPSESSID=' + header
          },
          success: function (res) {
            that.setData({
              note: res.data
            })
          }
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },


  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.data.page++;
    this.load()
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})