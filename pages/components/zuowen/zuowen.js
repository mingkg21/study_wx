// pages/components/zuowen/zuowen.js
var R_htmlToWxml = require('../../../utils/htmlToWxml.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {

    zbiaoti: "2018军考文化统考作文题目",
    jiebiaoti: "从生长军(警)官招生中的大专毕业生士兵专升本考生",
    neirong: "当前,“传承红色基因，担当强军重任”主题教育活动正在如火如茶地开展,强军是每一名革命军人的神圣使命。请围绕“强军”这一话题,写一篇不少于800字的作文,题目自拟,问题不限,诗歌除外。",
    id: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.data.id = options.id
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onShow: function() {
    var app = getApp();
    var httpUrl = app.globalData.httpUrl;
    var header;
    var that = this;
    wx.getStorage({
      key: 'login',
      success: function (res) {
        header = res.data;
        console.log(res)
      }
    })
    wx.login({
      success:((res)=>{
        if(res.code) {
          wx.request({
            url: httpUrl + 'app/wxapi.php?i=3&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
            data: {
              age: 247,
              id: this.data.id
            },
            header: {
              'Cookie': 'PHPSESSID=' + header
            },
            success: ((res) => {
              var html = R_htmlToWxml.html2json(res.data.content)
              that.setData({
                html: html,
                name: res.data.name,
                riqi: res.data.addtime
              })
            }),
            fail: ((res) => {
              console.log(res);
            })
          })
        }
      })
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  // //图片缩放
  // imageload: function (e) {
  //   var img = e.currentTarget.dataset.src;
  //   console.log(img);
  //   wx.previewImage({
  //     urls: [img],
  //   })
  // }
})