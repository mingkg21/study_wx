// pages/components/dizhi/dizhi.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    region: ['广东省', '广州市', '海珠区'],
    customItem: '全部',
    userName: '',
    userphone: '',
    xxdz1: '',
    arrid: '',
    title: '',
    id: '',
    thumb: '',
    productprice: '',
    marketprice1: ''
  },
  userphone: function (e) {

    this.setData({
      userphone: e.detail.value
    })
  },
  bindDateChange: function (e) {
    var userName = this.data.userName;
    var mobile = this.data.userphone;
    var phonetel = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1})|(17[0-9]{1}))+\d{8})$/;
    var name = /^[u4E00-u9FA5]+$/;
    if (mobile == '') {
      wx.showToast({
        title: '请输入用户名',
        icon: 'succes',
        duration: 1000,
        mask: true
      })
    }
    this.setData({
      date: e.detail.value
    })
  },
  bindRegionChange: function (e) {
    // console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      region: e.detail.value
    })
  },
  userNameInput: function (e) {
    // console.log(e.detail.value)设置用户名
    this.setData({
      userName: e.detail.value
    })
  },
  xxdz: function (e) {
    console.log(e)
    this.setData({
      xxdz1: e.detail.value
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var marketprice1 = options.marketprice1
    console.log(options)
    var arrid = options.arrid;
    var title = options.title;
    var id = options.id;
    var thumb = options.thumb;
    var productprice = options.productprice;
    this.setData({
      arrid: arrid,
      productprice: productprice,
      thumb: thumb,
      id: id,
      title: title,
      marketprice1: marketprice1
    })
  },
  shangchuan: function (e) {
    var title = this.data.title;
    var id = this.data.id;
    var thumb = this.data.thumb;
    var productprice = this.data.productprice;
    var arrid = this.data.arrid;
    var marketprice1 = this.data.marketprice1;
    var phone = this.data.userphone;
    if (phone == '') {
      wx.showToast({
        title: '请输入手机号码',
        icon: 'succes',
        duration: 1000,
        mask: true
      })
    } else {
      if (arrid == 1) {
        wx.redirectTo({
          url: '../ljgmxiangqing/ljgmxiangqing?arrid=' + 1 + '&title=' + title + '&thumb=' + thumb + '&productprice=' + productprice + '&id=' + id + '&marketprice1=' + marketprice1,
        })
      } else {
        wx.switchTab({
          url: '../user/user',
        })
      }
      var app = getApp();
      var header;
      var httpUrl = app.globalData.httpUrl;
      var that = this;
      wx.getStorage({
        key: 'login',
        success: function (res) {
          header = res.data
          wx.request({
            url: httpUrl + 'app/wxapi.php?i=3&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
            data: {
              age: 234,
              realname: that.data.userName,
              mobile: that.data.userphone,
              address: that.data.xxdz1,
              province: that.data.region,
            },
            header: {
              "Content-Type": "applciation/json",
              'Cookie': 'PHPSESSID=' + header
            },
            success: function (res) {
              console.log(res);
            }
          })
        }
      })
    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function (e) {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})