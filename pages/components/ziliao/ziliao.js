var sliderWidth = 100; // 需要设置slider的宽度，用于计算中间位置

Page({
  data: {
    duration: 1000,
    indicatorDots: true,
    autoplay: true,
    interval: 5000,
    page: 1,
    num: '',
    imgUrls: [],
    cateid:''
  },
  onLoad: function () {
    var page = this.data.page;
    var num = this.data.num
    var app = getApp();
    var header;
    var httpUrl = app.globalData.httpUrl;
    var src = app.globalData.src;
    var that = this;
    that.setData({
      src: src
    })
    wx.getStorage({
      key: 'login',
      success: function (res) {
        header = res.data
        wx.request({
          url: httpUrl + 'app/wxapi.php?i=2&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
          data: {
            age: 20
          },
          header: {
            "Content-Type": "applciation/json",
            'Cookie': 'PHPSESSID=' + header
          },
          success: function (res) {
            var res1 = res.data.adv_data;
            for (var i = 0; i < res1.length; i++) {
              that.setData({
                imgsra: res1,
              })
            }
          }

        })
        wx.request({
          url: httpUrl + 'app/wxapi.php?i=2&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
          data: {
            age: 261,
            num: num,
            type1: 1,
            page: page,
            cateid: 0
          },
          success: function (res) {
            that.setData({
              kecheng: res.data.data
            })
          }
        })
        wx.request({
          url: httpUrl + 'app/wxapi.php?i=2&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
          data: {
            age: 286,
          },
          header: {
            "Content-Type": "applciation/json",
            'Cookie': 'PHPSESSID=' + header
          },
          success: function (res) {

            that.setData({
              fenlei: res.data
            })
          }
        })
        //资料轮播图
        wx.request({
          url: httpUrl +'app/wxapi.php?i=2&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
          data:{
            age:268
          },
          header: {
            "Content-Type": "applciation/json",
            'Cookie': 'PHPSESSID=' + header
          },
          success:function(res) {
            console.log(res);
            that.setData({
              imgUrls: res.data
            })
          }
        }) 
      }
    })
  },
  onReachBottom: function () {
    var app = getApp();
    var httpUrl = app.globalData.httpUrl;
    var that = this;
    var num = this.data.num;
    var kecheng1 = this.data.kecheng
    var cateid = this.data.cateid
    // wx.showLoading({
    //   title: '加载中',
    // })

    that.setData({
      page: this.data.page + 1
    })
    wx.request({
      url: httpUrl + 'app/wxapi.php?i=2&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
      data: {
        age: 261,
        page: this.data.page,
        num: 5,
        type1: 1,
        cateid: cateid
      },
      success: function (res) {

        that.setData({
          kecheng: kecheng1.concat(res.data.data)
        })
      }
    })
    var query = wx.createSelectorQuery();
    var that = this;
    query.select('#mjltest').boundingClientRect(function (res) {

      that.setData({
        height: res.height
      })
    }).exec()
  },
  tiaozhuanzhiliao: function (obj) {
    console.log(obj)
    var that = this;
    var zlid = obj.currentTarget.dataset.id;
    that.setData({
      goodsid: obj.currentTarget.dataset.id
    })
    wx.navigateTo({
      url: '../zlxq/zlxq?zlid=' + zlid
    })
  },
  switchNav(event) {
    var cateid = event.currentTarget.dataset.id;
    this.setData({
      cateid: cateid
    })
    var page = this.data.page;
    var that = this;
    var app = getApp();
    var header;
    var httpUrl = app.globalData.httpUrl;

    wx.getStorage({
      key: 'login',
      success: function (res) {
        wx.request({
          url: httpUrl + 'app/wxapi.php?i=2&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
          data: {
            age: 261,
            page: 1,
            type1: 1,
            cateid: cateid,
          },
          header: {
            'Cookie': 'PHPSESSID=' + header
          },
          success: function (res) {

            that.setData({
              kecheng: res.data.data
            })
          }
        })
      }
    })
    var cur = event.currentTarget.dataset.current;
    //每个tab选项宽度占1/5

    //tab选项居中                            
    
    if (this.data.currentTab == cur) {
      return false;
    } else {
      this.setData({
        currentTab: cur
      })
    }
  },
  onShow:function(){
   
  }
});