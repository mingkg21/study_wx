// pages/components/huiyuan/huiyuan.js
Page({
  /**
   * 页面的初始数据
   */
  data: {
    code: ''
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    var app = getApp();
    var header;
    var httpUrl = app.globalData.httpUrl;
    var src = app.globalData.httpUrl;
    var that = this;
    that.setData({
      src: src
    })
    wx.getStorage({
      key: 'login',
      success: function (res) {
        header = res.data
        wx.request({
          url: httpUrl + 'app/wxapi.php?i=3&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
          data: {
            age: 266,
          },
          header: {
            'Cookie': 'PHPSESSID=' + header
          },
          success: function (res) {
            // console.log(res)
            that.setData({
              shuliang: res.data.data,
              dengji: res.data.vip
            })
          }
        })
      }
    })
  },
  lijigoumai: function (options) {
    // console.log(options)
    var app = getApp();
    var header;
    var httpUrl = app.globalData.httpUrl;
    var src = app.globalData.httpUrl;
    var that = this;
    var id = options.currentTarget.dataset.id
    wx.getStorage({
      key: 'login',
      success: function (res) {
        header = res.data
        wx.request({
          url: httpUrl + 'app/wxapi.php?i=3&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
          data: {
            age: 267,
            id: id
          },
          header: {
            "Content-Type": "applciation/json",
            'Cookie': 'PHPSESSID=' + header
          },
          success: function (res) {
            console.log(res.data.code)
            var message = res.data.message;
            if (res.data.code == 0) {
              wx.showModal({
                title: message,
                confirmText: "确定",
                cancelText: "取消",
              })
            } else {
              wx.requestPayment({
                'timeStamp': res.data.timeStamp,
                'nonceStr': res.data.nonceStr,
                'package': res.data.package,
                'signType': 'MD5',
                'paySign': res.data.paySign,
                'success': function (obj) {
                  wx.redirectTo({
                    url: '../huiyuan/huiyuan',
                  })
                },
                'fail': function (res) {

                }
              })
            }
          }
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },
  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})