var sliderWidth = 96;
var that = this;
var R_htmlToWxml = require('../../../utils/htmlToWxml.js')
Page({
  data: {
    src1: '', //这里放视频链接
    fenmian: '../../../utils/img/kc.png', //这是放视频封面图片
    tabs: ["课程详情", "章节目录", "相关课程"],
    activeIndex: 0,
    sliderOffset: 0,
    sliderLeft: 0,
    sliderWidth: 0,
    items: {
      is_like: 0,
      like: 0
    },
    AtB3: '',
    list: [{
      id: 'form',
      name: '词法',
      open: false,
      pages: ['button']
    },],
    videourl: '',
    videourq: '',
    vid: '',
    kongzishij: '',
    zhedang: 0,
    is_collect: '',
    goodsid3: '',
    fufei: '',
    page: 1,
    shuliang: ''
  },
  ljgm: function () {

    var app = getApp();
    var header;
    var httpUrl = app.globalData.httpUrl;
    var AtB3 = this.data.AtB3
    var that = this;
    wx.getStorage({
      key: 'login',
      success: function (res) {
        header = res.data;
        wx.request({
          url: httpUrl + 'app/wxapi.php?i=3&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
          data: {
            age: 245,
            goodsid: that.data.vid
          },
          header: {
            'Cookie': 'PHPSESSID=' + header
          },
          success: function (res) {

            wx.requestPayment({
              'timeStamp': res.data.timeStamp,
              'nonceStr': res.data.nonceStr,
              'package': res.data.package,
              'signType': 'MD5',
              'paySign': res.data.paySign,
              'success': function (obj) {
                var app = getApp();
                var httpUrl = app.globalData.httpUrl;


                wx.request({
                  url: httpUrl + 'app/wxapi.php?i=3&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
                  data: {
                    age: 251,
                    id: AtB3
                  },
                  header: {
                    'Cookie': 'PHPSESSID=' + header
                  },
                  success: function (res) {

                    that.setData({
                      fufei: 1
                    })
                    console.log('2222' + that.data.fufei)
                    // var id = AtB3;
                    // wx.redirectTo({
                    //   url: '../xiangqing/xiangqing?arrid='+id,
                    // })
                  }
                })
              },
              'fail': function (res) {
                console.log("2" + JSON.stringify(res))
              },
            })
            // console.log(res);
          }
        })
      }
    })
  },
  nihao: function () {
    wx.makePhoneCall({
      phoneNumber: '4006240068',
    })
  },
  onReady: function (res) {

  },
  onReachBottom() {
    var page = this.data.page;
    this.setData({
      page: page + 1
    })
    var httpUrl = getApp().globalData.httpUrl;
    var AtB3 = this.data.AtB3;
    var that = this;
    var header;
    var shuliang = this.data.shuliang
    wx.getStorage({
      key: 'login',
      success: function (res) {
        header = res.data;
        wx.request({
          url: httpUrl + 'app/wxapi.php?i=3&c=site&a=entry&m=ewei_shopv2&do=app&r=students.thumb',
          data: {
            age: 21,
            id: AtB3,
            page: page + 1
          },
          header: {
            'Cookie': 'PHPSESSID=' + header
          },
          success: function (res) {
            that.setData({
              shuliang: shuliang.concat(res.data.vide_data)
            })
          }
        })
      }
    })
  },
  shiping: function () {
    var that = this;
    // console.log(that.data.videourq);
    that.setData({
      videourl: that.data.videourq
    })
  },
  kindToggle: function (e) {
    var is_like = "items.is_like"; //先用一个变量，把items.is_like用字符串拼接起来
    var like = "items.like";
    if (this.data.items.is_like == 0 && this.data.items.like == 0) {
      this.setData({
        [is_like]: 1,
        [like]: 1
      })
    } else if (this.data.items.is_like == 1 && this.data.items.like == 1) {

      this.setData({

        [is_like]: 0,

        [like]: 0

      })

    }
    var id = e.currentTarget.id,
      list = this.data.list;
    for (var i = 0, len = list.length; i < len; ++i) {
      if (list[i].id == id) {
        list[i].open = !list[i].open
      } else {
        list[i].open = false
      }
    }
    this.setData({
      list: list
    });
  },
  chifa: function () {

  },
  onLoad: function (options) {
    console.log(options)
    this.videoContext = wx.createVideoContext('myVideo')
    var that = this;
    var src = getApp().globalData.src;
    that.setData({
      src: src
    })
    var goodsid3 = options.arrid;
    var app = getApp();
    var header;
    var httpUrl = app.globalData.httpUrl;
    var AtB3 = this.data.AtB3
    that.setData({
      AtB3: goodsid3
    })
    wx.getStorage({
      key: 'login',
      success: function (res) {
        header = res.data;
        wx.request({
          url: httpUrl + 'app/wxapi.php?i=3&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
          data: {
            age: 251,
            id: options.arrid
          },
          header: {
            'Cookie': 'PHPSESSID=' + header
          },
          success: function (res) {

            that.setData({
              fufei: res.data.code,
              fufei1: res.data.code
            })
          }
        })

        wx.request({
          url: httpUrl + 'app/wxapi.php?i=3&c=site&a=entry&m=ewei_shopv2&do=app&r=students.thumb',
          data: {
            age: 21,
            id: goodsid3,

          },
          header: {
            'Cookie': 'PHPSESSID=' + header
          },
          success: function (res) {
            var content = res.data.course_detail.content;
            var xiang = R_htmlToWxml.html2json(content)
            console.log(xiang)
            that.setData({
              xiang: xiang,
              marketprice: res.data.course_detail.marketprice,
              title: res.data.course_detail.title,
              content: res.data.course_detail.content,
              buy_number: res.data.course_detail.buy_number,
              hour: res.data.course_detail.hour,
              xgmarketprice: res.data.course_data,
              videourl: res.data.vide_data,
              moren: res.data.vide_data['0'].videourl,
              vid: res.data.vide_data[0].vid,
              is_collect: res.data.is_collect,
              //videourq: res.data.vide_data[1].videourl,
              shuliang: res.data.vide_data,
              kongzishij: res.data.course_data[0].freetime
            })
          }
        })
      }
    })


    var that = this;
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          sliderLeft: (res.windowWidth / that.data.tabs.length - sliderWidth) / 2,
          sliderOffset: res.windowWidth / that.data.tabs.length * that.data.activeIndex
        });
      }
    });


  },
  huiyuabn: function () {
    wx.navigateTo({
      url: '../huiyuan/huiyuan',
    })
  },
  dianshouc: function (res) {
    var is_collect = this.data.is_collect
    var goodsid3 = this.data.AtB3
    if (is_collect == 1) {
      wx.showToast({
        title: '取消收藏',
        icon: 'success',
        duration: 3000,
        success: function (res) { }
      })
      this.setData({
        is_collect: 0
      })
    } else {
      wx.showToast({
        title: '收藏成功',
        icon: 'success',
        duration: 3000,
        success: function (res) {
          var app = getApp();
          var httpUrl = app.globalData.httpUrl;
          var header;
          wx.getStorage({
            key: 'login',
            success: function (res) {
              header = res.data;
              wx.request({
                url: httpUrl + 'app/wxapi.php?i=3&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
                data: {
                  age: 252,
                  id: goodsid3
                },
                header: {
                  'Cookie': 'PHPSESSID=' + header
                },
                success: function (res) {
                  console.log(res)
                }
              })
            }
          })
        }

      })
      this.setData({
        is_collect: 1
      })
    }
  },
  huanyg: function (obj) {
    var that = this;
    console.log(obj.currentTarget.dataset.free)
    var free = obj.currentTarget.dataset.free;
    var sping = obj.currentTarget.dataset.url;
    if (free == 0) {
      return false
    } else {
      that.setData({
        moren: sping
      })
    }
  },
  funtimeupdate: function (u) {
    // console.log(u)
    var that = this;
    var shijian = u.detail.currentTime
    var kongzishij = that.data.kongzishij
    var fufei = that.data.fufei
    console.log(that.data)
    if (fufei == 0) {
      if (shijian >= kongzishij) {
        wx.showModal({
          content: '请购买后观看',
          showCancel: false,
          success: function (res) {
            if (res.confirm) {
              console.log('用户点击确定')
            }
          }
        });
        this.videoContext.pause();
        this.setData({
          zhedang: 1
        })
      } else {
        //试看中
      }
    } else {

    }
  },
  //控制视频播放
  kongzhibof: function (obj) {
    console.log(obj)
  },
  xiangqing: function (obj) {
    console.log(obj)
    var id = obj.currentTarget.dataset.id;
    wx.navigateTo({
      url: '../xiangqing/xiangqing?arrid=' + id
    })
  },
  tabClick: function (e) {
    this.setData({
      sliderOffset: e.currentTarget.offsetLeft,
      activeIndex: e.currentTarget.id
    });
  },
  videoErrorCallback: function (e) {
    console.log('视频错误信息:')
    console.log(e.detail.errMsg)
  },
  switchNav(event) {
    var cur = event.currentTarget.dataset.current;
    //每个tab选项宽度占1/5
    var singleNavWidth = this.data.windowWidth / 5;
    //tab选项居中                            
    this.setData({
      navScrollLeft: (cur - 2) * singleNavWidth
    })
    if (this.data.currentTab == cur) {
      return false;
    } else {
      this.setData({
        currentTab: cur
      })
    }
  },
  zuijxue: function (res) {
    var app = getApp();
    var header;
    var httpUrl = app.globalData.httpUrl;
    var goodsid3 = this.data.AtB3
    wx.getStorage({
      key: 'login',
      success: function (res) {
        header = res.data;
        wx.request({
          url: httpUrl + 'app/wxapi.php?i=3&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
          data: {
            age: 262,
            id: goodsid3
          },
          header: {
            'Cookie': 'PHPSESSID=' + header
          },
          success: function (res) {
          }
        })
      }
    })
  },
  switchTab(event) {
    var cur = event.detail.current;
    var singleNavWidth = this.data.windowWidth / 5;
    this.setData({
      currentTab: cur,
      navScrollLeft: (cur - 2) * singleNavWidth
    });
  }
})