// pages/components/zlxq/zlxq.js
var R_htmlToWxml = require('../../../utils/htmlToWxml.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    wjzt: "武警真题：包含2012-2017年真题详解，共计语文/数学/英语/物理/化学/政治/六个学科。每科6套题，共计36套题。需要的战友请直接拍下付款。默认发申通。如果申通无法到达，需要发送顺丰和EMS请联系客服改价！",
    zlid: '',
    title: "",
    marketprice: '',
    thumb: '',
    id: '',
    is_collect: '',
    xiang: [],
    marketprice1: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this;
    // console.log(options);
    var zlid = options.zlid;
    that.setData({
      zlid: zlid
    })
    var app = getApp();
    var header;
    var httpUrl = app.globalData.httpUrl;
    var src = getApp().globalData.src
    that.setData({
      src: src
    })

    wx.getStorage({
      key: 'login',
      success: function(res) {
        header = res.data;
        wx.request({
          url: httpUrl + 'app/wxapi.php?i=3&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
          data: {
            age: 233,
            goodsid: options.zlid
          },
          header: {
            'Cookie': 'PHPSESSID=' + header
          },
          success: function(res) {
            console.log(res);
            var content = res.data.content;
            var xiang = R_htmlToWxml.html2json(content)
            console.log(R_htmlToWxml)
            that.setData({
              xiang: xiang,
              title: res.data.title,
              thumb: res.data.thumb,
              id: res.data.id,
              marketprice: res.data.marketprice,
              productprice: res.data.productprice,
              subtitle: res.data.subtitle,
              content: res.data.content,
              is_collect: res.data.is_collect,
              buy_number: res.data.buy_number,
              marketprice1: res.data.marketprice1
            })
          }
        })
      }
    })
  },
  dianshouc: function(res) {
    var is_collect = this.data.is_collect
    var goodsid3 = this.data.zlid
    if (is_collect == 1) {
      wx.showToast({
        title: '取消收藏',
        icon: 'success',
        duration: 3000,
        success: function(res) {}
      })
      this.setData({
        is_collect: 0
      })
    } else {
      wx.showToast({
        title: '收藏成功',
        icon: 'success',
        duration: 3000,
        success: function(res) {
          var app = getApp();
          var httpUrl = app.globalData.httpUrl;
          var header;
          wx.getStorage({
            key: 'login',
            success: function(res) {
              header = res.data;
              wx.request({
                url: httpUrl + 'app/wxapi.php?i=3&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
                data: {
                  age: 252,
                  id: goodsid3
                },
                header: {
                  'Cookie': 'PHPSESSID=' + header
                },
                success: function(res) {

                }
              })
            }
          })
        }

      })
      this.setData({
        is_collect: 1
      })
    }
  },
  zhifu: function() {
    var title = this.data.title;
    var productprice = this.data.marketprice;
    var thumb = this.data.thumb;
    var id = this.data.id;
    var marketprice1 = this.data.marketprice1
    wx.navigateTo({
      url: '../ljgmxiangqing/ljgmxiangqing?title=' + title + '&productprice=' + productprice + '&thumb=' + thumb + '&id=' + id + '&marketprice1=' + marketprice1,
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  // 电话
  open: function(event) {
    console.log(111);
    wx.makePhoneCall({
      phoneNumber: '0595-22285595',
    })
  },
  //钻石会员
  huiyuan: function() {
    wx.navigateTo({
      url: '../huiyuan/huiyuan',
    })
  },
  //图片缩放
  imageload:function(e) {
    var img = e.currentTarget.dataset.src;
    console.log(img);
    wx.previewImage({
      urls: [img],
    })
  }
})