// pages/components/sz/sz.js
var util = require('../../../utils/util.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    neir: '',
    time: '',
    timer: '请输入考试日期'
  },

  tijiao: function() {
    var httpUrL = getApp().globalData.httpUrl;
    var timer = this.data.timer;
    var header
    wx.getStorage({
      key: 'login',
      success: function(res) {
        header = res.data;
        wx.request({
          url: httpUrL + 'app/wxapi.php?i=3&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
          data: {
            age: 236,
            timer: timer
          },
          header: {
            'Cookie': 'PHPSESSID=' + header
          },
          success: function(res) {
            wx.showToast({
              title: '修改完成',
              icon: 'success',
              duration: 3000
            });
          }
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var time = util.formatTime(new Date());

    console.log(time)
    this.setData({
      time: time
    })
  },
  bindDateChange: function(e) {
    var timer = e.detail.value;
    this.setData({
      timer: timer
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})