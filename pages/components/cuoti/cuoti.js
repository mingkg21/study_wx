var sliderWidth = 96; // 需要设置slider的宽度，用于计算中间位置

Page({
  data: {
    tabs: ["考点练习", "历年真题", "仿真试题"],
    activeIndex: 0,
    sliderOffset: 0,
    sliderLeft: 0,
    wenben: "化学",
    jlks: "328",
    open: true,
    jj: false,
    items: {
      is_like: 0,
      like: 0
    },
    bian: false,
    zbian: false,
    yixia: false
  },
  //第一级
  dianyixia: function(e) {
    var item = e.currentTarget.dataset.item;
    if (item.son) {
      var yixia = this.data.yixia

      if (yixia == false) {
        this.setData({
          yixia: true
        })
      } else {
        this.setData({
          yixia: false
        })
      }
      var jj = this.data.jj
      if (jj == false) {
        this.setData({
          jj: true
        })
      } else {
        this.setData({
          jj: false
        })
      }
      var bian = this.data.bian
      if (bian == false) {
        this.setData({
          bian: true
        })
      } else {
        this.setData({
          bian: false
        })
      }
    } else {
      wx.navigateTo({
        url: '../mryl/mryl?name=ctcl&libid=' + item.id
      })
    }
  },
  limdian: function(e) {
    var item = e.currentTarget.dataset.item;
    if (item.son) {
      var zbian = this.data.zbian
      if (zbian == false) {
        this.setData({
          zbian: true
        })
      } else {
        this.setData({
          zbian: false
        })
      }
    } else {
      wx.navigateTo({
        url: '../mryl/mryl?name=ctcl&libid=' + item.id
      })
    }
  },
  onReady: function() {
    this.load(0)
  },
  load(index) {
    var that = this;
    var app = getApp();
    var header;
    var httpUrl = app.globalData.httpUrl;
    var index = index
    wx.getStorage({
      key: 'login',
      success: function(res) {
        header = res.data;
        wx.request({
          url: httpUrl + 'app/wxapi.php?i=3&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
          data: {
            age: 226,
            etype: index
          },
          header: {
            'Cookie': 'PHPSESSID=' + header
          },
          success: function(res) {
            switch (Number(index)) {
              case 0:
                that.setData({
                  error_total: res.data.num,
                  list1: res.data.wrong_data,
                })
                break;
              case 1:
                that.setData({
                  error_total: res.data.num,
                  shuliang: res.data.data,
                })
                break;
              case 2:
                that.setData({
                  error_total: res.data.num,
                  fzst: res.data.data,
                })
                break;
            }
          }
        })
      }
    })
  },
  
  tabClick: function(e) {
    this.setData({
      sliderOffset: e.currentTarget.offsetLeft,
      activeIndex: e.currentTarget.id,
      shuliang: [],
      list1: [],
      fzst: []
    });
    this.load(e.currentTarget.id);
  },
  kaos: function(e) {
    var id = e.currentTarget.dataset.id
    switch (Number(this.data.activeIndex)) {
      case 1:
        wx.navigateTo({
          url: '../mryl/mryl?id=' + id + '&name=ctln' + '&examtype=' + 1,
        })
        break;
      case 2:
        wx.navigateTo({
          url: '../mryl/mryl?id=' + id + '&name=ctfz' + '&examtype=' + 2,
        })
        break;
    }
  },
  
  three: function(e) {
    var item = e.currentTarget.dataset.item
    wx.navigateTo({
      url: '../mryl/mryl?name=ctcl&libid=' + item.id
    })
  },
});