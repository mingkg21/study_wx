// pages/components/user/user.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    user_name: "E_ven_",
    zhiye: "边消警-化学",
    icoic: `../../../utils/img/kc.png`,
    nickname: ''
  },
  open: function(event) {
    wx.makePhoneCall({
      phoneNumber: '0595-22285595',
    })
  },
  navgetto: function() {
    // wx.navigateTo({
    //   url: '../bzxz/bzxz?iid=' + 0,
    // })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

    var app = getApp();
    var header;
    var httpUrl = app.globalData.httpUrl;
    var that = this;
    wx.getStorage({
      key: 'user_info',
      success: function(res) {
        if (res.data) {
          that.setData({
            nickname: res.data.nickname,
            avatar: res.data.avatar,
            vip: res.data.vip,
            exam_name: res.data.major.name
          })
        } else {
          wx.getStorage({
            key: 'login',
            success: function(res) {
              header = res.data;
              wx.request({
                url: httpUrl + '/app/wxapi.php?i=3&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
                data: {
                  age: 140
                },
                header: {
                  'Cookie': 'PHPSESSID=' + header
                },
                success: function(res) {
                  that.setData({
                    nickname: res.data.nickname,
                    avatar: res.data.avatar,
                    vip: res.data.vip,
                    exam_name: res.data.major.name
                  })
                  wx.setStorage({
                    key: 'user_info',
                    data: res.data,
                  })
                }
              })
            }
          })
        }
      },
    })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})