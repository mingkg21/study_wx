var sliderWidth = 96; // 需要设置slider的宽度，用于计算中间位置

var app = getApp()
Page({
  data: {
    tabs: ["视频", "资料", '待发货', '待收货', '已完成'],
    navbar: ['待发货', '待收货', '已完成'],
    currentTab: 0,
    nums: ["0", "1"],
    activeIndex: 0,
    sliderOffset: 0,
    sliderLeft: 0,
    xians: 0,
    hah: 0,
  },
  navbarTap: function(e) {
    this.setData({
      currentTab: e.currentTarget.dataset.idx
    })
  },
  onLoad: function() {
    var that = this;
    var src = getApp().globalData.src;
    that.setData({
      src: src
    })
    wx.getSystemInfo({
      success: function(res) {
        that.setData({
          sliderLeft: (res.windowWidth / that.data.tabs.length - sliderWidth) / 2,
          sliderOffset: res.windowWidth / that.data.tabs.length * that.data.activeIndex
        });
      }
    });
  },
  tabClick: function(e) {
    var xians = this.data.xians
    if (xians == 0) {
      this.setData({
        xians: 1
      })
    } else {
      this.setData({
        xians: 0
      })
    }
    this.setData({
      sliderOffset: e.currentTarget.offsetLeft,
      activeIndex: e.currentTarget.id
    });
  },
  quxiao_digdan: function(e) {
    console.log(e)
    var that = this;
    var app = getApp();
    var header;
    var httpUrl = app.globalData.httpUrl;
    wx.getStorage({
      key: 'login',
      success: function(res) {
        header = res.data;
        wx.request({
          url: httpUrl + 'app/wxapi.php?i=3&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
          data: {
            age: 250,
            orderid: e.currentTarget.dataset.id
          },
          header: {
            'Cookie': 'PHPSESSID=' + header
          },
          success: function(res) {
            console.log(res)
            wx.showModal({
              title: '提示',
              content: '确认取消订单吗',
              confirmText: "是",
              cancelText: "否",
              success: function(res) {
                if (res.confirm) { //这里是点击了确定以后
                  wx.navigateTo({
                    url: '../my_order/my_order'
                  })
                } else { //这里是点击了取消以后
                  wx.showToast({
                    title: '取消失败',
                    icon: 'success',
                    duration: 3000
                  });
                }
              }
            });
          }
        })
      }
    })

  },
  tiaozhuan: function(obj) {
    var id = obj.currentTarget.dataset.id
    wx.navigateTo({
      url: '../xiangqing/xiangqing?arrid=' + id,
    })
  },
  zhifu: function(obj) {
    var that = this;
    var app = getApp();
    var header;
    var httpUrl = app.globalData.httpUrl;
    var orderid = obj.currentTarget.dataset.id
    wx.getStorage({
      key: 'login',
      success: function(res) {
        header = res.data;
        wx.request({
          url: httpUrl + 'app/wxapi.php?i=3&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
          data: {
            age: 245,
            orderid: obj.currentTarget.dataset.id
          },
          header: {
            'Cookie': 'PHPSESSID=' + header
          },
          success: function(res) {
            console.log(res)
            wx.requestPayment({
              'timeStamp': res.data.timeStamp,
              'nonceStr': res.data.nonceStr,
              'package': res.data.package,
              'signType': 'MD5',
              'paySign': res.data.paySign,
              'success': function(obj) {
                // console.log("123213"+obj)
              },
              'fail': function(res) {
                var that = this;
                var app = getApp();
                var httpUrl = app.globalData.httpUrl;
                // console.log(res)
                wx.request({
                  url: httpUrl + 'app/wxapi.php?i=3&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
                  data: {
                    age: 246,
                    errMsg: res.errMsg
                  },
                  success: function(res) {
                    //  
                  }
                })
              }
            })
            // console.log(res);
          }
        })
      }
    })

  },

  tioatiao: function(obj) {
    var zlid = obj.currentTarget.dataset.id
    wx.navigateTo({
      url: '../zlxq/zlxq?zlid=' + zlid,
    })
  },
  onReady: function() {
    var app = getApp();
    var header;
    var httpUrl = app.globalData.httpUrl;
    var that = this;
    wx.getStorage({
      key: 'login',
      success: function(res) {
        header = res.data;
        
        wx.request({
          url: httpUrl + 'app/wxapi.php?i=3&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
          header: {
            'Cookie': 'PHPSESSID=' + header
          },
          data: {
            age: 249
          },
          success: function(res) {
            console.log(res)
            that.setData({
              shuliang: res.data.class_data,
              
             
            })
          }
        })
        wx.request({
          url: httpUrl + 'app/wxapi.php?i=3&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
          header: {
            'Cookie': 'PHPSESSID=' + header
          },
          data: {
            age: 249
          },
          success: function (res) {
            console.log(res)
            that.setData({
              zlshuliang: res.data.book_data.pay,
              finish: res.data.book_data.finish,
              gain: res.data.book_data.gain,
              send: res.data.book_data.send,
            })
          }
        })
      }
    })
  }
});