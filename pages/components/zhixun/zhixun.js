// pages/components/zhixun/zhixun.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    currentTab: 0,
  },
  switchNav(event) {
    var cateid = event.currentTarget.dataset.id;
    this.setData({
      cateid: cateid
    })
    var page = this.data.page;
    var that = this;
    var id = event.currentTarget.dataset.id
    var app = getApp();
    var header;
    var httpUrl = app.globalData.httpUrl;

    wx.getStorage({
      key: 'login',
      success: function (res) {
        header = res.data
        wx.request({
          url: httpUrl + 'app/wxapi.php?i=3&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
          data: {
            age: 281,
            page: 1,
            cateid: id,
          },
          header: {
            'Cookie': 'PHPSESSID=' + header
          },
          success: function (res) {
            console.log(res)
            that.setData({
              zixun: res.data.data
            })
          }
        })
      }
    })
    var cur = event.currentTarget.dataset.current;
    //每个tab选项宽度占1/5
    var singleNavWidth = this.data.windowWidth / 5;
    //tab选项居中                            
    this.setData({
      navScrollLeft: (cur - 2) * singleNavWidth
    })
    if (this.data.currentTab == cur) {
      return false;
    } else {
      this.setData({
        currentTab: cur
      })
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    var header;
    var httpUrl = getApp().globalData.httpUrl;
    var src = getApp().globalData.src
    that.setData({
      src: src
    })
    wx.getStorage({
      key: 'login',
      success: function (res) {
        header = res.data
        wx.request({
          url: httpUrl + 'app/wxapi.php?i=3&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
          data: { age: 280 },
          header: {
            'Cookie': 'PHPSESSID=' + header
          },
          success: function (res) {

            that.setData({
              shuliang: res.data
            })
          }
        })
        wx.request({
          url: httpUrl + 'app/wxapi.php?i=3&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
          data: {
            age: 281,
            page: 1,
            cateid: 0,
          },
          header: {
            'Cookie': 'PHPSESSID=' + header
          },
          success: function (res) {
            console.log(res)
            that.setData({
              zixun: res.data.data
            })
          }
        })
      }
    })
  },
  dianjijinq: function (res) {
    var id = res.currentTarget.dataset.id
    wx.navigateTo({
      url: '../zuowen/zuowen?id=' + id,
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})