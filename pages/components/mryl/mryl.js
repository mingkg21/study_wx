var sliderWidth = 96; // 需要设置slider的宽度，用于计算中间位置
var app = getApp();
var R_htmlToWxml = require('../../../utils/htmlToWxml.js')
var Utils = require('../../../utils/util.js');
Page({
  data: {
    tabs: ["选项一", ],
    activeIndex: 1,
    sliderOffset: 0,
    sliderLeft: 0,
    isChecked: true,
    dui: 0,
    cuo: 0,
    countDownNum: '0',
    countDowntime: '00:00',
    xingxing: ["123", "123", "123", "123", "12312"],
    cfbytj: false,
    parameter1: [1, 2, 3, 5, 3, 32, 424, 2],
    currentData: 0,
    pageImage: `../../../utils/img/wujiaoxing2.png`,
    pageImage1: `../../../utils/img/wujiaoxing1.png`,
    showView: false,
    isClick: false,
    isClick2: false,
    isClick3: false,
    isClick5: false,
    state: false,
    gengduo: false,
    clickId: 0,
    id: '',
    libid: '',
    xuande: [{}],
    data1: [],
    num: '',
    id1: '',
    cuukey: null,
    ids: [],
    num1: null,
    tid: '',
    allAnswer: false,
    selindex: null,
    findex: null,
    question: ['单选题','多选题','填空题','判断题','名词解释','简答题','论述题','案例分析','问答题','活动设计','材料题']
  },
  bindchange: function(e) {
    const that = this;
    var query = wx.createSelectorQuery();
    var num1 = Utils.arr.getEleCount(this.data.ids, e.detail.current)
    var collect = this.data.data1[e.detail.current].is_collect > 0 ? true : false
    if (num1 > 0) {
      that.setData({
        currentData: e.detail.current,
        cuukey: e.detail.current,
        isClick: true,
        num1: num1,
        isClick2: collect
      })
    } else {
      that.setData({
        cuukey: e.detail.current,
        currentData: e.detail.current,
        isClick: false,
        num1: num1,
        isClick2: collect
      })
    }
    query.selectAll('.swiper_con').boundingClientRect(function(rect) {
      that.setData({

        winHeight: rect[e.detail.current].height
      })
    }).exec();
  },

  fan: function(event) {
    var that = this;
    var id = event.currentTarget.dataset.id;
    var gengduo = that.data.gengduo;
    var clickId = that.data.clickId
    if (gengduo == false) {
      that.setData({
        gengduo: true
      })
    } else {
      that.setData({
        gengduo: true
      })
    }
    that.setData({
      currentData: event.currentTarget.dataset.id,
      clickId: event.currentTarget.dataset.id - 0,
    })
  },
  onReady: function() {
    console.log('suucess');
    var app = getApp();
    var header;
    var httpUrl = app.globalData.httpUrl;
    wx.getStorage({
      key: 'login',
      success: function(res) {
        header = res.data;
      }
    })
    var httpUrl = getApp().globalData.httpUrl;
    var that = this;
    var libid = this.data.libid;
    var id = this.data.id1;
    if (id) {
      libid = ''
    }
    wx.getSystemInfo({
      success: function(res) {
        console.log(res);
        var windowWidth = res.windowWidth;
        var windowHeight = res.windowHeight;
        var windowscale = windowHeight / windowWidth; //屏幕高宽比 
        that.setData({
          windowWidth: windowWidth,
          windowHeight: windowHeight,
          windowscale: windowscale
        })
      },
    })
    wx.login({
      success: function(res) {
        that.setData({
          code: res.code
        })
        wx.request({
          url: httpUrl + 'app/wxapi.php?i=2&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
          data: {
            age: 225,
            libid2: libid,
            id: id
          },
          header: {
            'Cookie': 'PHPSESSID=' + header
          },
          success: function(res) {

            var data = res.data
            var arr = res.data;
            for (var index in res.data.type) {
              res.data.type[index] = res.data.type[index].split(',');
            }
            console.log('data: ' + data)
            console.log('num: ' + data.num)
            res.data.data.forEach((d, index) => {
              var ind = index
              d.analysis = R_htmlToWxml.html2json(d.analysis)
              d.analysis.forEach((d1, index) => {
                console.log('index: ' + ind + '; d1: ' + d1.type)
                if (d1.type == 'img') {
                  wx.getImageInfo({
                    src: d1.attr.src,
                    success(res) {
                      var originalWidth = res.width; //图片原始宽 
                      var originalHeight = res.height; //图片原始高 
                      var originalScale = originalHeight / originalWidth; //图片高宽比 
                      if (originalScale < that.data.windowscale) { //图片高宽比小于屏幕高宽比 
                        //图片缩放后的宽为屏幕宽 
                        d1.attr.width = that.data.windowWidth - 20;
                        d1.attr.height = (that.data.windowWidth * originalHeight) / originalWidth;
                      }
                    }
                  })
                }
              })
              d.title = R_htmlToWxml.html2json(d.title);
              d.title.forEach((d1, index) => {
                console.log('title: ' + ind + '; d1: ' + d1.type)
                if (d1.type == 'img') {
                  wx.getImageInfo({
                    src: d1.attr.src,
                    success(res) {
                      var originalWidth = res.width; //图片原始宽 
                      var originalHeight = res.height; //图片原始高 
                      var originalScale = originalHeight / originalWidth; //图片高宽比 
                      if (originalScale < that.data.windowscale) { //图片高宽比小于屏幕高宽比 
                        //图片缩放后的宽为屏幕宽 
                        d1.attr.width = that.data.windowWidth - 20;
                        d1.attr.height = (that.data.windowWidth * originalHeight) / originalWidth;
                      }
                    }
                  })
                }
              })
              if (d.options) {
                d.options.forEach((data, i) => {
                  console.log('id: ' + d.id)
                  console.log('options: ' + ind + '; options: ' + d.options)
                  data.value = R_htmlToWxml.html2json(data.value);
                  console.log('options: ' + ind + '; value: ' + data.value)
                  data.value.forEach((d1, index) => {
                    console.log('options: ' + ind + '; d1: ' + d1.type)
                    if (d1.type == 'img') {
                      wx.getImageInfo({
                        src: d1.attr.src,
                        success(res) {
                          var originalWidth = res.width; //图片原始宽 
                          var originalHeight = res.height; //图片原始高 
                          var originalScale = originalHeight / originalWidth; //图片高宽比 
                          if (originalScale < that.data.windowscale) { //图片高宽比小于屏幕高宽比 
                            //图片缩放后的宽为屏幕宽 
                            if (res.width < that.data.windowWidth) {
                              if (res.width + 50 > that.data.windowWidth) {
                                d1.attr.width = that.data.windowWidth - 50
                              } else {
                                d1.attr.width = res.width;
                              }
                            } else {
                              d1.attr.width = that.data.windowWidth - 50
                            }
                          } else {
                            d1.attr.width = res.width
                          }
                        }
                      })
                    }
                  })
                })
              }
              if (res.data.data.length - 1 == index) {
                that.setData({
                  num: res.data.num
                })
              }
            })
            setTimeout((da) => {
              that.setData({
                data1: res.data.data
              })
              console.log(that.data.data1);
              var query = wx.createSelectorQuery();
              query.selectAll('.swiper_con').boundingClientRect(function(rect) {
                that.setData({
                  winHeight: rect[0].height
                })
              }).exec();
            }, 2000)

          }
        })
      }
    })
  },
  openToast: function() {
    var app = getApp();
    var header;
    var httpUrl = app.globalData.httpUrl;
    wx.getStorage({
      key: 'login',
      success: function(res) {
        header = res.data;
      }
    })
    var httpUrl = getApp().globalData.httpUrl;
    var that = this;
    wx.login({
      success: function(res) {
        that.setData({
          code: res.code
        })
        wx.request({
          url: httpUrl + 'app/wxapi.php?i=2&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
          data: {
            age: 238,
            id: that.data.data1[that.data.currentData].id
          },
          header: {
            'Cookie': 'PHPSESSID=' + header
          },
          success: function(res) {
            if (res.data == 1) {
              that.data.data1[that.data.currentData].is_collect = 1;
              that.setData({
                isClick2: true,
                data1: that.data.data1
              })
              wx.showToast({
                title: '已收藏',
                icon: 'success',
                duration: 3000,
              })
            } else {
              wx.showToast({
                title: '取消收藏',
                icon: 'success',
                duration: 3000,
              })
              that.data.data1[that.data.currentData].is_collect = 0;
              that.setData({
                isClick2: false,
                data1: that.data.data1
              })
              wx.hideLoading()
            }
          }
        })
      }
    })
  },
  // 点击提交试卷
  openConfirm: function() {
    var that = this;
    var xuande = this.data.xuande
    var question = that.data.data1;
    wx.showModal({
      title: '是否提交考题',
      confirmText: "确定",
      cancelText: "取消",
      success: function(res) {
        if (res.confirm) {
          var httpUrl = getApp().globalData.httpUrl;
          var that = this;

          var answer = [];
          for (var i = 0; i < question.length; i++) {
            if (question[i].iid) {
              var answerItem = {
                id: question[i].id,
                answer: question[i].iid,
                cateid: question[i].type
              };
            } else {
              var answerItem = {
                id: question[i].id,
                answer: question[i].value,
                cateid: question[i].type
              };
            }
            answer.push(answerItem)
          }
          var header;
          var httpUrl = getApp().globalData.httpUrl;
          var that = this;
          wx.getStorage({
            key: 'login',
            success: function(res) {
              header = res.data;
              wx.request({
                url: httpUrl + 'app/wxapi.php?i=2&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
                data: {
                  age: 239,
                  data: answer
                },
                header: {
                  'Cookie': 'PHPSESSID=' + header
                },
                success: function(res) {
                  wx.redirectTo({
                    url: '../jieguo/jieguo',
                  })
                }
              })
            }
          })
        } else {}
      }
    });
  },
  openTpast3: function() {
    this.setData({
      isClick3: !this.data.isClick3
    })
    if (!this.data.isChecked3 == false) {
      this.setData({
        isChecked3: false
      })
    } else {
      this.setData({
        isChecked3: true
      })
    }
  },
  openToast2: function() {
    var index = this.data.ids.findIndex((d, index) => {
      return d == this.data.currentData
    })
    var num;
    if (index >= 0) {
      this.data.ids.splice(index, 1)
      num = 0;
    } else {
      this.data.ids.push(this.data.currentData)
      num = Utils.arr.getEleCount(this.data.ids, this.data.currentData)
    }
    this.setData({
      num1: num,
      id: this.data.id,
      cuukey: this.data.currentData,
      isClick: !this.data.isClick
    })

  },
  //获取当前滑块的index
  parameterTap: function(e) { //e是获取e.currentTarget.dataset.id所以是必备的，跟前端的data-id获取的方式差不多

    var dui = this.data.dui
    var xuande = e.currentTarget.dataset.id
    var that = this;
    var index = that.data.currentData;
    var id = e.currentTarget.dataset.index;
    var indexa = e.currentTarget.dataset.indexa;
    var valuu = e.detail.value;
    var that = this;
    var xuande = this.data.xuande
    var question = that.data.data1;
    var num = this.data.num

    //单选题
    if (that.data.data1[index].type == 1) {
      that.data.data1[index].iid = id;
      that.setData({
        key: e.currentTarget.dataset.index,
        selindex: e.currentTarget.dataset.index,
      })
    }
    //判断题
    else if (that.data.data1[index].type == 4) {
      that.data.data1[index].iid = e.currentTarget.dataset.findex;
      // that.data.data1[index].iid = id;
      // that.setData({
      //   findex: e.currentTarget.dataset.findex,
      // })
    }
    //简答题
    else if (that.data.data1[index].type == 3) {
      that.data.data1[index].value = valuu;
      that.setData({
        key: e.currentTarget.dataset.index,
        selindex: e.currentTarget.dataset.index,
        data1: that.data.data1
      })
    }
    //多选题
    else if (that.data.data1[index].type == 2) {
      if (!that.data.data1[index].iid) {
        that.data.data1[index].iid = [];
      }
      var num1 = Utils.arr.getEleCount(that.data.data1[index].iid, id)
      if (num1 > 0) {
        var index2 = that.data.data1[index].iid.findIndex((d, index) => {
          return d == id
        })
        that.data.data1[index].iid.splice(index2, 1)
        that.data.data1[index].options[indexa].checked = false
      } else {
        that.data.data1[index].iid.push(id);
        that.data.data1[index].options[indexa].checked = true
      }
    } else {
      that.data.data1[index].iid = id;
    }
    if (dui == 0) {
      that.setData({
        data1: that.data.data1,
        dui: 1,
        cuo: 0,
      })
    } else {
      that.setData({
        dui: 0,
        cuo: 1
      })
    }
    that.setData({
      data1: that.data.data1,
    })
    setTimeout(() => {
      {
        if (that.data.data1[index].type == 1 || that.data.data1[index].type == 4) {
          var currentData = that.data.currentData + 1;
          if (currentData >= num) {
            wx.showModal({
              title: '是否提交考题',
              confirmText: "确定",
              cancelText: "取消",
              success: function(res) {
                if (res.confirm) {
                  var httpUrl = getApp().globalData.httpUrl;
                  var that = this;

                  var answer = [];
                  for (var i = 0; i < question.length; i++) {
                    if (question[i].iid) {
                      var answerItem = {
                        id: question[i].id,
                        answer: question[i].iid,
                        cateid: question[i].type
                      };
                    } else {
                      var answerItem = {
                        id: question[i].id,
                        answer: question[i].value,
                        cateid: question[i].type
                      };
                    }
                    answer.push(answerItem)
                  }
                  var header;
                  var httpUrl = getApp().globalData.httpUrl;
                  var that = this;
                  wx.getStorage({
                    key: 'login',
                    success: function(res) {
                      header = res.data;
                      wx.request({
                        url: httpUrl + 'app/wxapi.php?i=2&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',
                        data: {
                          age: 239,
                          data: answer
                        },
                        header: {
                          'Cookie': 'PHPSESSID=' + header
                        },
                        success: function(res) {
                          wx.navigateTo({
                            url: '../jieguo/jieguo',
                          })
                        }
                      })
                    }
                  })
                } else {}
              }
            });
          } else {
            that.setData({
              currentData: currentData,
              selindex: e.currentTarget.dataset.index,
            })
          }
          if (currentData === e.target.dataset.current) {

            return false;

          } else {

            that.setData({

            })
          }
        }
      }
    }, 500)
  },
  onShow: function() {

    this.countDown();
  },
  countDown: function() {
    let that = this;
    let countDownNum = that.data.countDownNum; //获取倒计时初始值
    var hour = '00';
    var second = '00';
    //如果将定时器设置在外面，那么用户就看不到countDownNum的数值动态变化，所以要把定时器存进data里面
    that.setData({
      timer: setInterval(function() { //这里把setInterval赋值给变量名为timer的变量
        //每隔一秒countDownNum就加一，实现同步
        countDownNum++;
        //然后把countDownNum存进data，好让用户知道时间在倒计着
        var tHour = Math.floor(countDownNum / 60);
        var tSecond = countDownNum % 60;
        if (tHour < 10) {
          hour = '0' + tHour;
        } else {
          hour = tHour;
        }
        if (tSecond < 10) {
          second = '0' + tSecond;
        } else {
          second = tSecond;
        }
        that.setData({
          countDownNum: countDownNum,
          countDowntime: hour + ':' + second
        })
        //在倒计时还未到0时，这中间可以做其他的事情，按项目需求来
        if (countDownNum == 0) {
          //这里特别要注意，计时器是始终一直在走的，如果你的时间为0，那么就要关掉定时器！不然相当耗性能
          //因为timer是存在data里面的，所以在关掉时，也要在data里取出后再关闭
          clearInterval(that.data.timer);
          //关闭定时器之后，可作其他处理codes go here
        }
      }, 1000)
    })
  },
  onLoad: function(options) {
    var that = this;
    that.setData({
      // libid: options.libid,
      libid: getApp().globalData.selectedLibId,
      id1: options.id
    })
    this.onReady();
    wx.showLoading({
      title: '加载中',
      duration: 3000
    })
    // wx.getSystemInfo({
    //   success: function(res) {
    //     that.setData({
    //       sliderLeft: (res.windowWidth / that.data.tabs.length - sliderWidth) / 2,
    //       sliderOffset: res.windowWidth / that.data.tabs.length * that.data.activeIndex
    //     });
    //   }
    // });
  },

  //点击切换，滑块index赋值

  tabClick2(e) {

    this.setData({
      allAnswer: true,
      activeIndex: Number(e.currentTarget.dataset.id)
    });

  },
  tabClick: function(e) {
    this.setData({
      allAnswer: false,
      activeIndex: Number(e.currentTarget.dataset.id)
    });
  }
});