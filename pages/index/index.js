//index.js
//获取应用实例
const app = getApp();
Page({
  data: {
    message: '[英语]2019年边消警《军考英语》考点精讲课程',
    logs: [],
    data1: '',
    indicatorDots: true,
    autoplay: true,
    circular: true,
    interval: 5000,
    duration: 1000,
    arrid: '',
    zlid: null,
    goodsid: '',
    libs: null,
    currentLibIndex: 0,
  },
  onCategoryClick: function(e) {
    var index = e.currentTarget.dataset['index'];
    if (index == 0 || index == 3) {
      wx.showToast({
        title: '即将开放，敬请期待',
        icon: 'none',
        duration: 2000
      })
    } else if (index == 1) {
      wx.navigateTo({
        url: '../components/mryl/mryl?name=mryl',
      })
    } else if (index == 2) {
      wx.navigateTo({
        url: '../components/fzst/fzst',
      })
    }
  },
  bindLibPickerChange(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    getApp().setSelectedLib(this.data.libs[e.detail.value])
    this.setData({
      currentLibIndex: e.detail.value
    })
  },
  tiao: function() {
    wx.navigateTo({
      url: '../components/zuowen/zuowen',
    })
  },
  shauti: function() {
    wx.switchTab({
      url: '../components/brush/brush',
    })
  },
  tozixun: function() {
    wx.navigateTo({
      url: '../components/zhixun/zhixun',
    })
  },
  tiaozhuan: function(obj) {

    var id = obj.currentTarget.dataset.id;
    wx.navigateTo({
      url: '../components/xiangqing/xiangqing?arrid=' + id
    })
  },
  zuowen: function(event) {

    var id = event.currentTarget.dataset.id
    wx.navigateTo({
      url: '../components/zuowen/zuowen?id=' + id,
    })
  },
  tiaozhuanzhiliao: function(obj) {

    var that = this;
    var zlid = obj.currentTarget.dataset.id;
    that.setData({
      goodsid: obj.currentTarget.dataset.id
    })
    wx.navigateTo({
      url: '../components/zlxq/zlxq?zlid=' + zlid
    })
  },
  //事件处理函数
  bindViewTap: function() {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },

  onLoad: function() {
    
    var src = getApp().globalData.src
    this.setData({
      src1: src
    })

    this.setData({
      libs: getApp().globalData.libs
    })

    var libs = this.data.libs
    console.log('selectedLibId: ' + getApp().globalData.selectedLibId)
    if (libs && getApp().globalData.selectedLibId) {
      for (var i = 0; i < libs.length; i++) {
        if (libs[i].id === getApp().globalData.selectedLibId) {
          this.setData({
            currentLibIndex: i
          })
        }
      }
    }

    var app = getApp();
    var header;
    var httpUrl = app.globalData.httpUrl;
    wx.getStorage({
      key: 'login',
      success: function(res) {
        header = res.data;
        console.log(res)
      }
    })
    wx.login({
      success: res => {
        var goodsid = this.data.goodsid
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
        if (res.code) {
          var goodsid = this.data.goodsid
          wx.request({

            url: httpUrl + 'app/wxapi.php?i=3&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',

            data: {
              code: res.code,
              goodsid: goodsid
            },
            header: {
              'Cookie': 'PHPSESSID=' + header
            },
            success: function(res) {
              var that = this;

              // getApp().globalData.header.Cookie = 'PHPSESSID=' + res.data;
              //  console.log(res);
              if (res.data.openid) {
                wx.setStorage({
                  key: "tokenId",
                  data: res.data.openid,
                })
              }
            }
          })

        }
      }
    })
    wx.setNavigationBarTitle({
      title: '名道题库',
    });
    if (app.globalData.userInfo) {
      this.setData({
        userInfo: app.globalData.userInfo,
        hasUserInfo: true
      })
    } else if (this.data.canIUse) {
      // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
      // 所以此处加入 callback 以防止这种情况
      app.userInfoReadyCallback = res => {
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    } else {
      // 在没有 open-type=getUserInfo 版本的兼容处理
      wx.getUserInfo({
        success: res => {
          app.globalData.userInfo = res.userInfo
          this.setData({
            userInfo: res.userInfo,
            hasUserInfo: true
          })
        }
      })
    }
  },

  onReady: function() {
    var httpUrl = getApp().globalData.httpUrl;
    var that = this;
    var header;
    wx.request({
      url: httpUrl + 'app/wxapi.php?i=3&c=site&a=entry&m=ewei_shopv2&do=app&r=students.thumb',

      data: {
        age: 20,

      },
      success: function(res) {

        that.setData({
          res: res.data,
          arr1: res.data.course_data,
          arr2: res.data.exam_data,
          arr3: res.data.article_data
        })
        var res1 = res.data.adv_data;
        for (var i = 0; i < res1.length; i++) {
          that.setData({
            imgsra: res1,
          })

        }
      }
    })
    wx.getStorage({
      key: 'login',
      success: function(res) {
        header = res.data;
        wx.request({
          url: httpUrl + 'app/wxapi.php?i=3&c=site&a=entry&m=ewei_shopv2&do=mobile&r=wxapp',

          data: {
            age: 140,
          },
          header: {
            'Cookie': 'PHPSESSID=' + header
          },
          success: function(res) {
            console.log('nickname: ' + res.data.nickname)
            console.log('avatar: ' + res.data.avatar)
            console.log('major: ' + res.data.major.name)
            wx.setStorage({
              key: 'user_info',
              data: res.data,
            })

            getApp().setLibs(res.data.subject)
            that.setData({
              libs: res.data.subject
            })

            if (getApp().globalData.selectedLibId == 0) {
              if (res.data.subject) {
                getApp().setSelectedLib(res.data.subject[0])
                that.setData({
                  currentLibIndex: 0
                })
              }
            }
          }
        })
      }
    })
  },
  // getUserInfo: function(e) {
  //   console.log(e)
  //   app.globalData.userInfo = e.detail.userInfo
  //   this.setData({
  //     userInfo: e.detail.userInfo,
  //     hasUserInfo: true
  //   })
  // },

  /**
     * 用户点击右上角分享
     */
  onShareAppMessage: function () {

  }
})